module gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2

go 1.15

require (
	github.com/Nerzal/gocloak/v8 v8.1.1
	github.com/grafana-tools/sdk v0.0.0-20210921191058-888ef9d18611
	github.com/pkg/errors v0.9.1
	github.com/sethvargo/go-password v0.2.0
	github.com/spf13/cobra v1.1.1
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20210409120209-2cf55852f3a4
	gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli v0.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
)
