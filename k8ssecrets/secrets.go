package k8ssecrets

import (
	"context"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const secretName = "grafana"

func GetGrafanaSecrets(grafanaNamespace string) (username string, password string) {

	k8sClient = initK8sClient()

	secret, err := k8sClient.CoreV1().Secrets(grafanaNamespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if err != nil {
		log.Fatalf("%v", err)
	}

	username = string(secret.Data["admin-user"])
	if username == "" {
		log.Fatalf("Missing username in secret %q", secretName)
	}
	password = string(secret.Data["admin-password"])
	if password == "" {
		log.Fatalf("Missing password in secret %q", secretName)
	}
	return username, password
}
