package tenant

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"github.com/Nerzal/gocloak/v8"
	"github.com/grafana-tools/sdk"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2/config"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

func members(grafanaClient *sdk.Client, organisation *config.OrganisationConfig, grafanaUsers map[string]*sdk.User, organizationID uint) error {

	log.Printf("Create members for organizationID %v if necessary ...", organizationID)

	orgUsers, err := grafanaClient.GetOrgUsers(context.TODO(), organizationID)
	if err != nil {
		return err
	}

	grafanaRolesWeight := map[string]uint{
		"admin":  30,
		"editor": 20,
		"viewer": 10,
	}

	//Sort grafana roles in order of significance
	sort.SliceStable(organisation.Roles, func(i, j int) bool {
		return grafanaRolesWeight[strings.ToLower(organisation.Roles[i].Name)] > grafanaRolesWeight[strings.ToLower(organisation.Roles[j].Name)]
	})

	processedUser := make(map[uint]bool)
	for _, role := range organisation.Roles {

		for _, grafanaUser := range grafanaUsers {
			if processedUser[grafanaUser.ID] {
				//only proces the most significant role
				continue
			}
			processedUser[grafanaUser.ID] = true

			userRole := sdk.UserRole{}
			userRole.Role = strings.Title(role.Name)

			orgMember := getOrgMember(orgUsers, grafanaUser)
			if orgMember == nil {
				userRole.LoginOrEmail = grafanaUser.Email
				log.Printf("Creating user %q with role %q", grafanaUser.Email, userRole.Role)
				statusMessage, err := grafanaClient.AddActualOrgUser(context.TODO(), userRole)
				if err != nil {
					return fmt.Errorf("AddActualOrgUser err: %+w", err)
				}
				log.Printf("User %q. Result: %q", grafanaUser.Email, *statusMessage.Message)
			} else {
				log.Printf("Existing user %q has organization role %q", orgMember.Email, orgMember.Role)
				if orgMember.Role != userRole.Role {
					log.Printf("Updating user %q to role %q", grafanaUser.Email, userRole.Role)
					statusMessage, err := grafanaClient.UpdateOrgUser(context.TODO(), userRole, orgMember.OrgId, orgMember.ID)
					if err != nil {
						return fmt.Errorf("UpdateOrgUser err: %+w", err)
					}
					log.Printf("User %q. Result: %q", grafanaUser.Email, *statusMessage.Message)
				}
			}
		}

	}
	return nil
}

func getOrgMember(orgUsers []sdk.OrgUser, grafanaUser *sdk.User) *sdk.OrgUser {
	for _, orgUser := range orgUsers {
		if orgUser.ID == grafanaUser.ID {
			return &orgUser
		}
	}
	return nil
}

func organization(grafanaClient *sdk.Client, organisation *config.OrganisationConfig) (uint, error) {
	org, err := grafanaClient.GetOrgByOrgName(context.TODO(), organisation.Name)
	if err != nil {
		// Continue if 404, the organization does not exist yet
		if !strings.Contains(err.Error(), "404") {
			return 0, err
		}
	}
	if org.Name == "" {
		return createOrganization(grafanaClient, organisation.Name)
	} else {
		return org.ID, nil
	}
}

func users(grafanaClient *sdk.Client, keyCloakClient *keycloak.Client, organisation *config.OrganisationConfig) (map[string]*sdk.User, error) {
	log.Printf("Create users if necessary ...")
	users := make(map[string]*sdk.User)

	log.Printf("Organisation: %v", organisation)
	for _, role := range organisation.Roles {
		for _, group := range role.Groups {
			groupMembers, err := getKeyCloakGroupMembers(keyCloakClient, group)
			if err != nil {
				return nil, err
			}

			keycloakUsers := getKeyCloakUsers(groupMembers)
			for _, keycloakUser := range keycloakUsers {
				user, err := getOrCreateGrafanaUser(grafanaClient, keyCloakClient, keycloakUser)
				if err != nil {
					return nil, err
				}
				users[*keycloakUser.Username] = user
			}
		}
	}
	return users, nil
}

func getKeyCloakGroupMembers(keyCloakClient *keycloak.Client, group string) (map[string][]*gocloak.User, error) {
	keycloakGroupMembers := make(map[string][]*gocloak.User)
	groupID, err := keyCloakClient.GetGroupID(group)
	if err != nil {
		return nil, err
	}
	if groupID == "" {
		return nil, fmt.Errorf("could not find group %q in KeyCloak", group)
	}
	members, err := keyCloakClient.GetGroupMembers(groupID)
	if err != nil {
		return nil, err
	}
	keycloakGroupMembers[group] = getProperConfiguredUsers(members)
	return keycloakGroupMembers, nil
}

func getProperConfiguredUsers(users []*gocloak.User) []*gocloak.User {
	properUsers := make([]*gocloak.User, 0)
	for _, user := range users {
		if strings.HasPrefix(*user.Username, "_") {
			log.Printf("Skip internal user %q", *user.Username)
		} else {
			properUsers = append(properUsers, user)
		}
	}
	return properUsers
}

func getOrCreateGrafanaUser(grafanaClient *sdk.Client, keyCloakClient *keycloak.Client, keycloakUser *gocloak.User) (*sdk.User, error) {
	grafanaUser, found, err := getUser(grafanaClient, keycloakUser)
	if err != nil {
		return nil, err
	}

	//If user is found return the user
	if found {
		return grafanaUser, nil
	}

	//Make a new account
	password, err := password.Generate(20, 5, 5, false, false)
	if err != nil {
		return nil, err
	}
	grafanaUser = &sdk.User{
		Login:    *keycloakUser.Email,
		Email:    *keycloakUser.Email,
		Name:     *keycloakUser.FirstName + " " + *keycloakUser.LastName,
		Password: password,
	}
	log.Printf("Create Grafana user %q", *keycloakUser.Email)
	statusMessage, err := grafanaClient.CreateUser(context.TODO(), *grafanaUser)
	if err != nil {
		log.Printf("CreateUser statusmessage %+v", statusMessage)
		return nil, err
	}
	grafanaUser.ID = *statusMessage.ID
	return grafanaUser, nil

}

func getKeyCloakUsers(keycloakGroupMembers map[string][]*gocloak.User) map[string]*gocloak.User {
	keycloakUsers := make(map[string]*gocloak.User)
	for _, members := range keycloakGroupMembers {
		for _, member := range members {
			keycloakUsers[*member.Username] = member
		}
	}
	return keycloakUsers
}

func getUser(grafanaClient *sdk.Client, keycloakUser *gocloak.User) (*sdk.User, bool, error) {
	grafanaUsers, err := grafanaClient.SearchUsersWithPaging(context.TODO(), keycloakUser.Email, nil, nil)
	if err != nil {
		return nil, false, err
	}
	for _, grafanaUser := range grafanaUsers.Users {
		if grafanaUser.Email == *keycloakUser.Email {
			return &grafanaUser, true, nil
		}
	}
	return &sdk.User{}, false, nil
}

func createOrganization(grafanaClient *sdk.Client, organizationName string) (uint, error) {
	org := sdk.Org{
		Name: organizationName,
	}
	log.Printf("Create Grafana organization %q", organizationName)
	statusMessage, err := grafanaClient.CreateOrg(context.TODO(), org)
	if err != nil {
		log.Printf("CreateOrg statusmessage %+v", statusMessage)
		return 0, err
	}
	return *statusMessage.OrgID, nil
}
