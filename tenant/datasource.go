package tenant

import (
	"context"
	"errors"
	"log"
	"os"
	"strings"

	"github.com/grafana-tools/sdk"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2/config"
)

var dsFuncMap = map[config.DatasourceType]func(datasource sdk.Datasource, customerConfig *config.Customer, dsConfig config.DatasourceConfig) (sdk.Datasource, error){
	config.Prometheus: addPrometheus,
	config.Elastic:    addElastic,
	config.Thanos:     addThanos,
}

type ServiceAccount struct {
	username string
	password string
}

const ELASTIC_READONLY_USER_NAME = "ELASTIC_READONLY_USER_NAME"
const ELASTIC_READONLY_PASSWORD = "ELASTIC_READONLY_PASSWORD"

func datasources(grafanaClient *sdk.Client, dataSources []config.DatasourceConfig, organizationID uint, customerConfig *config.Customer) error {

	err := removeUnnecesaryDatasources(grafanaClient, dataSources, organizationID)
	if err != nil {
		return err
	}

	for id, dataSource := range dataSources {

		ds := sdk.Datasource{
			OrgID: organizationID,
			// in Grafana GUI proxy is server access, direct is browser access. Furthermore, direct is also considered deprecated
			Access:    "proxy",
			Name:      dataSource.Name,
			URL:       dataSource.URL,
			IsDefault: id == 0, // first datasource will be default
		}
		if dsFunc, ok := dsFuncMap[dataSource.Type]; ok {
			var err error = nil
			ds, err = dsFunc(ds, customerConfig, dataSource)
			if err != nil {
				return err
			}
		}

		// check if datasource already exists
		log.Printf("Find datasource by name %v", ds.Name)
		existingDatasource, err := grafanaClient.GetDatasourceByName(context.TODO(), ds.Name)
		if err != nil {
			if !strings.Contains(err.Error(), "404") {
				return err
			}
			// If 404: continue, the datasource is just not there yet
		}
		if existingDatasource.ID != 0 {
			ds.ID = existingDatasource.ID
			log.Printf("Update datasource %v for organizationID %v", ds.Name, ds.OrgID)
			statusMessage, err := grafanaClient.UpdateDatasource(context.TODO(), ds)
			if err != nil {
				log.Printf("UpdateDatasource statusmessage: %+v", statusMessage)
				return err
			}
		} else {
			log.Printf("Create datasource %v for organizationID %v", ds.Name, ds.OrgID)
			statusMessage, err := grafanaClient.CreateDatasource(context.TODO(), ds)
			if err != nil {
				log.Printf("CreateDatasource statusmessage: %+v", statusMessage)
				return err
			}
		}
	}
	return nil
}

func removeUnnecesaryDatasources(grafanaClient *sdk.Client, datasources []config.DatasourceConfig, organizationID uint) error {
	existingDatasources, err := grafanaClient.GetAllDatasources(context.TODO())
	if err != nil {
		return err
	}

	for _, source := range existingDatasources {
		if source.OrgID != organizationID {
			continue
		}
		if !datasourceExists(source, datasources) {
			log.Printf("Removing Datasource %v it is no longer present in config", source.Name)
			err := deleteDatasource(grafanaClient, source.ID)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func datasourceExists(source sdk.Datasource, datasources []config.DatasourceConfig) bool {
	for _, ds := range datasources {
		if ds.Name == source.Name {
			return true
		}
	}
	return false
}

func deleteDatasource(grafanaClient *sdk.Client, datasourceID uint) error {
	_, err := grafanaClient.DeleteDatasource(context.TODO(), datasourceID)
	return err
}

// Prometheus is a very simple datasource to add, hence why it only need ssuch
func addPrometheus(datasource sdk.Datasource, customerConfig *config.Customer, dsConfig config.DatasourceConfig) (sdk.Datasource, error) {
	datasource.Type = "prometheus"
	return datasource, nil
}

func addThanos(datasource sdk.Datasource, customerConfig *config.Customer, dsConfig config.DatasourceConfig) (sdk.Datasource, error) {
	datasource.Type = "prometheus"
	datasource.JSONData = map[string]string{
		"httpHeaderName1": "THANOS-TENANT",
	}

	log.Printf("Thanos Clusters Added: %s\n", strings.Join(dsConfig.ClusterIdentifiers, "|"))

	datasource.SecureJSONData = map[string]string{
		"httpHeaderValue1": strings.Join(dsConfig.ClusterIdentifiers, "|"),
	}
	return datasource, nil
}

func addElastic(datasource sdk.Datasource, customerConfig *config.Customer, dsConfig config.DatasourceConfig) (sdk.Datasource, error) {
	serviceAcc, err := getServiceAccount(datasource.URL, customerConfig)
	if err != nil {
		return datasource, err
	}

	enableBasicAuth := true
	indices := "*"
	datasource.Type = "elasticsearch"
	datasource.BasicAuth = &enableBasicAuth
	datasource.BasicAuthUser = &serviceAcc.username
	datasource.SecureJSONData = map[string]interface{}{"basicAuthPassword": &serviceAcc.password}
	datasource.Database = &indices

	datasource.JSONData = map[string]string{
		"esVersion":                  "8.0.0",
		"logLevelField":              "",
		"logMessageField":            "",
		"maxConcurrentShardRequests": "5",
		"timeField":                  "@timestamp",
	}

	return datasource, nil
}

func getServiceAccount(URL string, customerConfig *config.Customer) (ServiceAccount, error) {
	gitlabURL := os.Getenv("GITLAB_URL")
	gitLabClient := gitlabclient.NewGitLabClient(gitlabURL)

	readOnlyUsername := ELASTIC_READONLY_USER_NAME
	readOnlyPassword := ELASTIC_READONLY_PASSWORD

	if strings.Contains(URL, "-prod") {
		readOnlyUsername += "_PROD"
		readOnlyPassword += "_PROD"
	}

	if strings.Contains(URL, "az2") {
		readOnlyUsername += "_AZ2"
		readOnlyPassword += "_AZ2"
	}

	for _, gitlabGroup := range customerConfig.GitLab.Groups {
		group, err := gitlabclient.GetGitLabGroup(gitLabClient, gitlabGroup.Name)
		if err != nil {
			return ServiceAccount{}, err
		}
		userName, _, _ := gitLabClient.GroupVariables.GetVariable(group.ID, readOnlyUsername)
		password, _, _ := gitLabClient.GroupVariables.GetVariable(group.ID, readOnlyPassword)
		if userName == nil || password == nil {
			return ServiceAccount{}, errors.New("username and/or password does not exist")
		} else {
			return ServiceAccount{username: userName.Value, password: password.Value}, nil
		}
	}
	return ServiceAccount{}, errors.New("failed to retrieve service Account")
}
