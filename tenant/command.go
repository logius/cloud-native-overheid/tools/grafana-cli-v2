package tenant

import (
	"context"
	"log"
	"os"

	"github.com/pkg/errors"

	"github.com/grafana-tools/sdk"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2/config"

	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2/k8ssecrets"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	config *string

	grafanaURL       *string
	grafanaNamespace *string
	keycloakURL      *string
	oidcRealm        *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			log.Printf("Start grafana-cli")
			err := configureTenant(cmd, &flags)
			if err != nil {
				return err
			} else {
				log.Printf("Done")
			}
			return nil
		},
		Use:   "configure-customer",
		Short: "Configure Grafana tenant",
		Long:  "This command configures organization for a tenant in Grafana.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.grafanaURL = cmd.Flags().String("grafana-url", "", "Grafana URL")
	flags.grafanaNamespace = cmd.Flags().String("grafana-namespace", "", "Grafana Namespace")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("grafana-url")
	cmd.MarkFlagRequired("keycloak-url")
	cmd.MarkFlagRequired("realm")

	return cmd
}

func configureTenant(cmd *cobra.Command, flags *flags) error {

	username, password := k8s.GetKeycloakSecrets()
	os.Setenv("KEYCLOAK_USERNAME", username)
	os.Setenv("KEYCLOAK_PASSWORD", password)

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	grafanaClient, err := createGrafanaClient(*flags.grafanaURL, *flags.grafanaNamespace)
	if err != nil {
		return err
	}

	log.Printf("Config: %v", customerConfig)

	for _, organisation := range customerConfig.Grafana.Organisations {
		// Get ID for organization and switch to that organization
		organizationID, err := organization(grafanaClient, &organisation)
		if err != nil {
			return err
		}
		statusMessage, err := grafanaClient.SwitchActualUserContext(context.TODO(), organizationID)
		if err != nil {
			log.Printf("SwitchActualUserContext statusMessage: %+v", statusMessage)
			return err
		}

		grafanaUsers, err := users(grafanaClient, keyCloakClient, &organisation)
		if err != nil {
			return err
		}

		if err := members(grafanaClient, &organisation, grafanaUsers, organizationID); err != nil {
			return err
		}

		if err := datasources(grafanaClient, organisation.Datasources, organizationID, customerConfig); err != nil {
			return err
		}
	}

	return nil
}

func createGrafanaClient(grafanaURL string, grafanaNamespace string) (*sdk.Client, error) {

	grafanaUser, grafanaPassword := k8ssecrets.GetGrafanaSecrets(grafanaNamespace)

	if grafanaUser == "" {
		return nil, errors.Errorf("Missing environment variable GRAFANA_USERNAME")
	}
	if grafanaPassword == "" {
		return nil, errors.Errorf("Missing environment variable GRAFANA_PASSWORD")
	}

	return sdk.NewClient(grafanaURL, grafanaUser+":"+grafanaPassword, sdk.DefaultHTTPClient)
}
