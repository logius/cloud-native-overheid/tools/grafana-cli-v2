package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// Customer configuration
type Customer struct {
	Grafana Grafana `validate:"required"`
	GitLab  GitLab  `validate:"required"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

type Grafana struct {
	Organisations []OrganisationConfig `validate:"required,dive"`
}

type OrganisationConfig struct {
	Name        string             `validate:"required"`
	Roles       []RoleConfig       `validate:"required,dive"`
	Datasources []DatasourceConfig `validate:"required,dive"`
}

type RoleConfig struct {
	Name   string   `validate:"required" yaml:"name"`
	Groups []string `validate:"required" yaml:"groups"`
}

type DatasourceConfig struct {
	Name               string         `validate:"required" yaml:"name"`
	Type               DatasourceType `validate:"required" yaml:"type",flow`
	URL                string         `validate:"required" yaml:"url"`
	ClusterIdentifiers []string       `yaml:"clusters"`
}

type DatasourceType string

const (
	Prometheus DatasourceType = "prometheus"
	Elastic    DatasourceType = "elastic"
	Thanos     DatasourceType = "thanos"
)

// GitLabGroup specifies a group in GitLab
type GitLabGroup struct {
	Name string `validate:"required"`
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroup `validate:"required,dive"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}

// Custom UnmarshalYAML extension to unmarshal the DatasourceType correctly and map the string to the enum.
func (s *DatasourceType) UnmarshalYAML(data []byte) error {
	// Define a secondary type so that we don't end up with a recursive call to json.Unmarshal
	type Aux DatasourceType
	var a *Aux = (*Aux)(s)
	err := yaml.Unmarshal(data, &a)
	if err != nil {
		return err
	}

	// Validate the valid enum values
	switch *s {
	case Prometheus, Elastic:
		return nil
	default:
		return errors.New("invalid value for DatasourceType")
	}
}
